package com.revolut.rates.base.mvp

/**
 * Created by servetcanasutay on 02/10/2018
 */

interface Presenter<V : BaseView> {

    fun attachView(view: V)

    fun detachView()
}