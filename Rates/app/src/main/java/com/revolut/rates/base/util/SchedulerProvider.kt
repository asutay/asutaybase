package com.revolut.rates.base.util

import io.reactivex.Scheduler

/**
 * Created by servetcanasutay on 02/10/2018
 */

interface SchedulerProvider {
    fun ui(): Scheduler
    fun computation(): Scheduler
    fun io(): Scheduler
}