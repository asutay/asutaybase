package com.revolut.rates.rest.model

/**
 * Created by servetcanasutay on 02/10/2018
 */

data class CurrencyRate(val currencyCode: String, var currencyRate: Double)