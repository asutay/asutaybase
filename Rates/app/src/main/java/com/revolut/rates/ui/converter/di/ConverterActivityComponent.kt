package com.revolut.rates.ui.converter.di

import com.revolut.rates.base.di.ActivityScope
import com.revolut.rates.base.di.component.AppComponent
import com.revolut.rates.ui.converter.ConverterActivity
import dagger.Component

/**
 * Created by servetcanasutay on 02/10/2018
 */

@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(ConverterActivityModule::class))
interface ConverterActivityComponent {

    fun inject(converterActivity: ConverterActivity)
}