package com.revolut.rates.ui.converter.di

import android.content.Context
import com.revolut.rates.base.di.ActivityScope
import com.revolut.rates.base.util.AppSchedulerProvider
import com.revolut.rates.rest.AppService
import com.revolut.rates.ui.converter.ConverterActivity
import com.revolut.rates.ui.converter.adapter.ConverterAdapter
import com.revolut.rates.ui.converter.presenter.ConverterPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by servetcanasutay on 02/10/2018
 */

@Module
class ConverterActivityModule(private val converterActivity: ConverterActivity) {

    @Provides
    @ActivityScope
    internal fun providesConverterActivity(): ConverterActivity {
        return converterActivity
    }

    @Provides
    @ActivityScope
    internal fun providesContext(): Context {
        return converterActivity
    }

    @Provides
    @ActivityScope
    internal fun providesConverterPresenter(api: AppService, disposable: CompositeDisposable, scheduler: AppSchedulerProvider): ConverterPresenter {
        return ConverterPresenter(api, disposable, scheduler)
    }

    @Provides
    @ActivityScope
    internal fun providesConverterAdapter() = ConverterAdapter()

}