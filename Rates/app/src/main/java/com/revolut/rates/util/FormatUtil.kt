package com.revolut.rates.util

import android.content.Context
import android.os.Build
import java.text.NumberFormat
import java.util.*

/**
 * Created by servetcanasutay on 03/10/2018
 */
class FormatUtil {

    fun currencyFormat(context: Context) : NumberFormat {
        val locale: Locale

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = context.getResources().getConfiguration().getLocales().get(0)
        } else {
            locale = context.getResources().getConfiguration().locale
        }

        val numberFormat = NumberFormat.getInstance(locale)
        numberFormat.minimumIntegerDigits = 1
        numberFormat.minimumFractionDigits = 2
        numberFormat.maximumFractionDigits = 2

        return numberFormat
    }
}