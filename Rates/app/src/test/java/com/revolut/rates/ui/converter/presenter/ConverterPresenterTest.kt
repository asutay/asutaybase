package com.revolut.rates.ui.converter.presenter

import com.revolut.rates.rest.AppService
import com.revolut.rates.rest.model.CurrencyRate
import com.revolut.rates.rest.model.RateResponse
import com.revolut.rates.util.TestSchedulerProvider
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.then
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations.initMocks


class ConverterPresenterTest {

    private companion object {
        private val SOME_CURRENCY_LIST = arrayListOf<CurrencyRate>()
        private val SOME_RATE_RESPONSE = mock(RateResponse::class.java)
    }

    @Mock
    private lateinit var api: AppService

    @Mock
    private lateinit var disposable: CompositeDisposable

    @Mock
    private lateinit var mockView: ConverterView

    private lateinit var testScheduler: TestScheduler

    private lateinit var subject: ConverterPresenter

    private var givenBaseCurrency = "EUR"

    @Before
    fun setUp() {
        initMocks(this)
        testScheduler = TestScheduler()

        subject = ConverterPresenter(api, disposable, TestSchedulerProvider(testScheduler)).apply {
            attachView(mockView)
        }
    }

    /**
     * TEST
     */
    @Test
    fun whenGetRates_givenSomeCurrencyRates_andApiResponseSuccess_thenCallsDisposableClear_andViewOnCurrenciesFetched() {
        givenSomeCurrencyRates()
        givenApiGetLatestIsSuccess()
        whenGetRates()
        thenCallsDisposableClear()
        thenViewOnCurrenciesFetched()
    }

    /**
     * GIVEN
     */
    private fun givenSomeCurrencyRates() {
        repeat(3) {
            SOME_CURRENCY_LIST.add(mock(CurrencyRate::class.java))
            given(SOME_CURRENCY_LIST[it].currencyCode).willReturn(givenBaseCurrency)
        }
    }

    private fun givenApiGetLatestIsSuccess() {
        doReturn(Observable.just(SOME_RATE_RESPONSE))
                .`when`(api)
                .getLatest(givenBaseCurrency)
    }

    /**
     * WHEN
     */
    private fun whenGetRates() {
        subject.getRates(SOME_CURRENCY_LIST)
        testScheduler.triggerActions()
    }

    /**
     * THEN
     */
    private fun thenCallsDisposableClear() {
        then(disposable).should().clear()
    }

    private fun thenViewOnCurrenciesFetched() {
        then(mockView).should().onCurrenciesFetched(SOME_CURRENCY_LIST)
    }
}